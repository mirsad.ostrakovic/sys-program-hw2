#include "error.h"


void systemCallError(const char *msg)
{
	char buff[128];

	strerror_r(errno, buff, 128);
	throw std::runtime_error(std::string(msg) + " : " + buff);
}


void fatalError(const char *msg)
{
	throw std::runtime_error(std::string(msg));
}

void reportError(const char *msg)
{
	fprintf(stderr, "%s\r\n", msg);
}

