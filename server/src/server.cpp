#include "server.h"

std::set<SocketInfo> ChatServer::userSockets;
std::mutex ChatServer::userSocketsMtx;
Loger ChatServer::loger(STDOUT_FILENO);

ChatServer::ChatServer(uint16_t port, bool mt):
	isMultithreading{mt}
{
	struct sockaddr_in serverAddr;

	if(signal(SIGPIPE, SIG_IGN) == SIG_ERR)
		systemCallError("ChatServer::ChatServer(): Can not turn off SIGPIPE error");

	if((serverFD = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		systemCallError("ChatServer::ChatServer(): Can not get server socket");

	memset(&serverAddr, 0, sizeof(serverAddr));

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(port);
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	if(bind(serverFD, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) == -1)
		systemCallError("ChatServer::ChatServer(): Server can not bind to port");

	if(listen(serverFD, SERVER_LISTEN_BACKLOG) == -1)
		systemCallError("ChatServer::ChatServer(): Server can not listen for connections");

	loger.writeLog("ChatServer::ChatServer(): compleated");

	serverMainLoop();
}




void ChatServer::serverMainLoop()
{
	int clientFD, maxFD;
	fd_set rfds;
	struct  timeval timeout;
	int rv;

	while(true)
	{

		FD_ZERO(&rfds);
		FD_SET(serverFD, &rfds);
		maxFD = serverFD;

		{	
			std::lock_guard<std::mutex> lock(userSocketsMtx);
			std::for_each(std::begin(userSockets), std::end(userSockets), [&](const SocketInfo& sockInfo){
							if(sockInfo.lock == false)
							{
								FD_SET(sockInfo.socketFD, &rfds);
								if(sockInfo.socketFD > maxFD) 
									maxFD = sockInfo.socketFD;
							}
					});	
		}

		//loger.writeLog("ChatServer::serverMainLoop(): waiting for 'select'");

		timeout.tv_sec = 0;
		timeout.tv_usec = 100000;

		if((rv = select(maxFD + 1, &rfds, NULL, NULL, &timeout)) == -1)
			systemCallError("ChatServer::serverMainLoop(): Server select on read sockets fail");

		
		for(int fd = 0; fd <= maxFD; ++fd)
			if(FD_ISSET(fd, &rfds))
			{
				if(fd == serverFD)
				{
					//loger.writeLog("ChatServer::serverMainLoop(): handle new connection");
					
					if(isMultithreading)
						std::thread(handleNewConnection, serverFD).detach();
					else
						handleNewConnection(serverFD);
				}
				else
				{	
					//loger.writeLog("ChatServer::serverMainLoop(): handle new message");
					
					if(isMultithreading)
						std::thread(handleNewMessage, fd).detach();
					else
						handleNewMessage(fd);
				}
			}
	
	}
}




void ChatServer::handleNewConnection(int fd)
{
	int clientFD;
	struct sockaddr userAddr;
	socklen_t userAddrLen = sizeof(struct sockaddr);

	if((clientFD = accept(fd, &userAddr, &userAddrLen)) == -1)
		systemCallError("ChatServer::handleNewConnection(): Server can not accept new connection");
	
	loger.userConnectedLog((struct sockaddr_in *)&userAddr);

	{
		std::lock_guard<std::mutex> lock(userSocketsMtx);
		userSockets.insert(SocketInfo(clientFD));
	}	
}


int ChatServer::lockSocket(int fd)
{
	std::unique_lock<std::mutex> lock(userSocketsMtx);
	
	for(auto it = std::begin(userSockets); it != std::end(userSockets); ++it)
		if(it->socketFD == fd)
		{
			if(it->lock == false)
			{
				it->lock = true;
				return SUCCESFULLY_LOCKED;
			}
			else
				return ALREADY_LOCKED;
		}
	
	return SOCKET_NOT_FIND;
}


int ChatServer::unlockSocket(int fd)
{	
	std::unique_lock<std::mutex> lock(userSocketsMtx);
	
	for(auto it = std::begin(userSockets); it != std::end(userSockets); ++it)
		if(it->socketFD == fd)
		{
			it->lock = false;
			return SUCCESFULLY_UNLOCKED;
		}
	
	return SOCKET_NOT_FIND;
}



void ChatServer::handleNewMessage(int fd)
{
	char msgBuff[BUFF_SZ];
	size_t msgLen;
	int rv;

	
	if(lockSocket(fd) != SUCCESFULLY_LOCKED)
		return;
	
	if((rv = Protocol::readMessage(fd, msgBuff, &msgLen)) != 0)
	{
		if(rv == SOCKET_CLOSED)
			closeSocket(fd);
		else
		{
			char buff[256];
			sprintf(buff, "%s %d", "ChatServer::handleNewMessage(): server can not read user message", rv);
			reportError(buff);
		}
		return;
	}

	unlockSocket(fd);


	if(ServerResponse::processMessage(msgBuff, &msgLen, fd) != 0)
	{
		reportError("ChatServer::handleNewMessage(): server can not process user message");
		return;
	}

	serverBroadcastMessage(msgBuff, msgLen);	
}





void ChatServer::closeSocket(int fd)
{
	std::lock_guard<std::mutex> lock(userSocketsMtx);
	
	for(auto it = std::begin(userSockets); it != std::end(userSockets); ++it)
		if(it->socketFD == fd)
		{
			userSockets.erase(it);
			break;
		}
}






void ChatServer::serverBroadcastMessage(const char *msgBuff, size_t msgLen)
{
	std::lock_guard<std::mutex> lock(userSocketsMtx);
	
	std::for_each(std::begin(userSockets), std::end(userSockets), [&](const SocketInfo& sockInfo){
						Protocol::sendMessage(sockInfo.socketFD, msgBuff, msgLen);		
				});	
}


