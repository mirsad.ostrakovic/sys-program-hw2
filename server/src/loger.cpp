#include "loger.h"

Loger::Loger(int fd):
	logerFD{fd}
{}



Loger::~Loger()
{
	close(logerFD);
}



void Loger::userConnectedLog(const struct sockaddr_in *userAddr)
{
	char buff1[32];
	char buff2[128];
	
	ServerResponse::getUserIPAddrHelper(userAddr->sin_addr, buff1);

	sprintf(buff2, "{User: %s %d}", buff1, ntohs(userAddr->sin_port));
	writeLog(buff2);
}



void Loger::userConnectedLog(int userFD)
{
	char buff1[32];
	char buff2[128];
	struct sockaddr_in userAddr;
	socklen_t userAddrLen;
	
	if(getpeername(userFD, (struct sockaddr *)&userAddr, &userAddrLen) == -1)
	{
		reportError("Loger::userConnectedLog(): getpeername fail");
		return;
	}

	ServerResponse::getUserIPAddrHelper(userAddr.sin_addr, buff1);

	sprintf(buff2, "{User: %s %d}", buff1, ntohs(userAddr.sin_port));
	writeLog(buff2);
}


void Loger::writeLog(const char *msg)
{
	static std::mutex mtx;
	std::lock_guard<std::mutex> lock(mtx);
	
	size_t n, pos = 0u;
	size_t msgLen = strlen(msg);

	while(true)
	{
		if((n = write(logerFD, &msg[pos], msgLen - pos)) == -1)
			systemCallError("Loger::writeLog(): write error");

		if(n == 0)
			fatalError("Loger::writeLog(): write 0 bytes");

		pos += n;
	
		if(pos == msgLen)
			break;
	}

	write(logerFD, "\r\n", 2);
}

