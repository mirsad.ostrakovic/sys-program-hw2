#include "server_response.h"

using namespace rapidjson;






void ServerResponse::getUserIPAddrHelper(struct in_addr userAddr, char *buff)
{ 	
	static std::mutex mtx;
	std::lock_guard<std::mutex> lock(mtx);
	char *ipAddr = inet_ntoa(userAddr);
	memcpy(buff, ipAddr, strlen(ipAddr) + 1);
}




int ServerResponse::getUserIPAddr(int fd, char *buff)
{
	struct sockaddr_in userAddr;
	socklen_t userAddrLen;
	
	if(getpeername(fd, (struct sockaddr *)&userAddr, &userAddrLen) == -1)
	{
		memcpy(buff, "unknown", 7);
		return -1;
	}

	getUserIPAddrHelper(userAddr.sin_addr, buff);
	return 0;
}





int ServerResponse::processMessage(char *msgBuff, size_t *msgLen, int fd)
{
	static const char msgFormat[] =   "{"    			
																					"\"user\""       ":" 	 "\"%s\""  ","
																					"\"message\""    ":" 	 "\"%s\""  ","
																					"\"ip\""    		 ":" 	 "\"%s\""  ","
																					"\"timestamp\""  ":" 	 "%d"
																		 "}";
	char ipAddrBuff[256];
	time_t currentTime;	
	Document document;

	msgBuff[*msgLen] = '\0';
	document.Parse(msgBuff);

	if(!document.IsObject() || !document.HasMember("message") || !document["message"].IsString())
	{
		reportError("ServerResponse::processMessage(): invalid user message");
		return -1;
	}

	time(&currentTime);
	getUserIPAddr(fd, ipAddrBuff);
	
	*msgLen = sprintf(msgBuff, msgFormat, "GENERIC_USER", 
		 																		 document["message"].GetString(), 
																				 ipAddrBuff, 
																				 currentTime);

	return 0;
}












/*
std::mutex inet_ntoa_mtx;

void inet_ntoa_safe(struct in_addr user_addr, char *buff)
{ 	
	std::lock_guard<std::mutex> lock(inet_ntoa_mtx);
	char *ip_addr = inet_ntoa(user_addr);
	memcpy(buff, ip_addr, strlen(ip_addr) + 1);
}

*/


/*
int get_user_ip_addr(int fd, char *buff)
{
	struct sockaddr_in user_addr;
	socklen_t user_addr_len;
	
	if(getpeername(fd, (struct sockaddr *)&user_addr, &user_addr_len) == -1)
	{
		memcpy(buff, "unknown", 7);
		return -1;
	}

	inet_ntoa_safe(user_addr.sin_addr, buff);
	return 0;
}
*/

/*
int process_msg(char *msg_buff, size_t *msg_len, int fd)
{
	static const char msg_format[] =   "{"    			
																					"\"user\""       ":" 	 "\"%s\""  ","
																					"\"message\""    ":" 	 "\"%s\""  ","
																					"\"ip\""    		 ":" 	 "\"%s\""  ","
																					"\"timestamp\""  ":" 	 "%d"
																		 "}";
	char ip_addr_buff[256];
	time_t current_time;	
	Document document;

	msg_buff[*msg_len] = '\0';
	document.Parse(msg_buff);

	if(!document.IsObject() || !document.HasMember("message") || !document["message"].IsString())
	{
		report_error("process_msg(): invalid user message");
		return -1;
	}

	time(&current_time);
	get_user_ip_addr(fd, ip_addr_buff);
	
	*msg_len = sprintf(msg_buff, msg_format, "GEN_USER", 
		 																			 document["message"].GetString(), 
																					 ip_addr_buff, 
																					 current_time);

	fprintf(stderr, "MSG: %.*s\r\n", *msg_len, msg_buff);

	return 0;
}
*/

