#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <cstring>

#include "error.h"
#include "server.h"

bool isNumber(const char *s)
{
	while(*s != '\0')
		if(!isdigit(*(s++)))
			return false;
	return true;
}


int main(int argc, const char *argv[])
{

	int port;
	bool isMultithreading = false;

	try
	{
		
		if(argc < 2 || argc > 3)
			fatalError("main(): invalid number of arguments, program expects 1 or 2 argument");

		if(!isNumber(argv[1]))
			fatalError("main(): first argument is not number, but expected to be port number");

		if((port = atoi(argv[1])) < 0 || port > 65535)	
			fatalError("main(): first argument is not port number, port number has to be positive number less than 65536");

		if(argc == 3)
			if(strcmp(argv[2], "-t") == 0)
				isMultithreading = true; 
			else
				fatalError("main(): second argument is not thread switch '-t'");
		
		ChatServer server(port, isMultithreading);

	}
	catch(std::exception& e)
	{
		std::cerr << "CriticalError: " << e.what() << std::endl;
	}

	return 0;
}


