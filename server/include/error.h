#ifndef ERROR_H
#define ERROR_H

#include <string>
#include <cstdio>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <stdexcept>
#include <string.h>

void systemCallError(const char *msg);
void fatalError(const char *msg);
void reportError(const char *msg);

#endif
