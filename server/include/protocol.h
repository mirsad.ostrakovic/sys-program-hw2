#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <algorithm>
#include <cstring>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

#include "error.h"

#define BUFF_SZ 1024
#define LENGTH_FIELD_SZ 5

#define  OK 0
#define  SOCKET_CLOSED -1
#define  FORMATTING_PROBLEM -2 
#define  INVALID_MSG_FORMAT -3	
#define  READING_DATA_ERROR -4

class Protocol {
	public:
		static int sendMessage(int fd, const char *msg, size_t msgLen);
		static int readMessage(int fd, char *msgBuff, size_t *msgLen);

	private:
		static int sendData(int fd, const char *msgBuff, size_t msgLen);
		static int readData(int fd, char *msgBuff, size_t msgLen);
};


#endif
