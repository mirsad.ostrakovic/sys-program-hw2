#ifndef SERVER_H
#define SERVER_H

#include <iostream>
#include <set>
#include <unordered_set>
#include <mutex>
#include <algorithm>
#include <cstring>
#include <thread>
#include <atomic>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>

#include "error.h"
#include "protocol.h"
#include "server_response.h"
#include "loger.h"

#define SERVER_LISTEN_BACKLOG 50

#define SUCCESFULLY_UNLOCKED 0
#define	SUCCESFULLY_LOCKED 	 0 
#define ALREADY_LOCKED  	  -1
#define SOCKET_NOT_FIND     -2

struct SocketInfo{

	SocketInfo(int fd): socketFD{fd}, lock{false} {}
	bool operator==(const SocketInfo& si) const { socketFD == si.socketFD; }
	bool operator<(const SocketInfo& si) const { socketFD < si.socketFD; }

	mutable bool lock;
	int socketFD;
};

//bool operator==(const SocketInfo& siLeft, const SocketInfo& siRight) { siLeft.socketFD == siRight.socketFD; }
//bool operator<(const SocketInfo& siLeft, const SocketInfo& siRight) { siLeft.socketFD < siRight.socketFD; }


class ChatServer{
	public:
		ChatServer(uint16_t port, bool isMultithreading);

	private:
		void serverMainLoop();	
		static int lockSocket(int fd);
		static int unlockSocket(int fd);
		static void handleNewConnection(int fd);
		static void handleNewMessage(int fd);
		static void closeSocket(int fd);
		static void serverBroadcastMessage(const char *msgBuff, size_t msgLen);

		static std::set<SocketInfo> userSockets;
		static std::mutex userSocketsMtx;
		static Loger loger;
		int serverFD;
		bool isMultithreading;
};


#endif
