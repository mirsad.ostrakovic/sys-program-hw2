#ifndef LOGER_H
#define LOGER_H

#include <mutex>
#include <unistd.h>
#include "server_response.h"
#include "error.h"

class Loger{
	public:
		Loger(int fd);
    ~Loger();	 	
		
		void writeLog(const char *msg);
		void userConnectedLog(int userFD);
		void userConnectedLog(const struct sockaddr_in *userAddr);

	private:
		int logerFD;	

};

#endif 
