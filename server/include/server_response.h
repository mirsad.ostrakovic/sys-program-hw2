#ifndef SERVER_RESPONSE_H
#define SERVER_RESPONSE_H

#include <mutex>
#include <ctime>
#include <cstdio>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "error.h"

class ServerResponse{
	public:
		static int processMessage(char *msgBuff, size_t *msgLen, int fd);	
		static int getUserIPAddr(int fd, char *buff);
		static void getUserIPAddrHelper(struct in_addr userAddr, char *buff);
};



#endif
