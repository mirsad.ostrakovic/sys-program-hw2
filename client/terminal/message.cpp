#include "message.h"

Message::Message(const std::string& u, const std::string& m, const time_t& t):
		user{u},
		message{m},
		timestamp{t}
{
	char buff[128];
	struct tm *time_info;

	time_info = gmtime(&timestamp);

	sprintf(buff, "%d:%d:%d %d.%d.%d", time_info->tm_hour, time_info->tm_min, time_info->tm_sec,
																		 time_info->tm_mday, time_info->tm_mon + 1, time_info->tm_year + 1900);
	time = buff;
}


