#include <set>
#include <vector>
#include <string>
#include <ctime>

#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#include "message.h"

struct termios orig_termios;
int ttyfd = STDIN_FILENO;

std::set<Message> messages;


void init_messages()
{
	time_t current_time;

	time(&current_time);
	messages.insert(Message("Mirsad", "1)Sta ima narodeee", current_time));

	time(&current_time);
	messages.insert(Message("Ostrakovic", "2)Kako ste raja", current_time + 1));

	time(&current_time);
	messages.insert(Message("Mirsad",
				"3)HAHAHAHAHAHAHAAHHAHAHAHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
				"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA CAAAAAAAAAAAAAAAAAAAAA MAIIIIIIIIIIIIIIII"
				"IIIIIIII LJEPOTE MOJEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
				" POZELIA SAM VAS MNOGOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
				"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",
				current_time + 2));

	time(&current_time);
	messages.insert(Message("Seka", "4)Ja sam Seka!", current_time + 3));
}




void fatal(char *message)
{
	perror(message);
	exit(-1);
}



void fatal2(char *message)
{
	fprintf(stderr, "%s", message);
	exit(-1);
}



ssize_t safe_read(int fd, void *buff, size_t count)
{
	ssize_t ret_val;

	while((ret_val = read(fd, buff, count)) == -1 && errno == EINTR);

	return ret_val;
}



ssize_t safe_write(int fd, const void *buff, size_t count)
{
	ssize_t ret_val;

	while((ret_val = write(fd, buff, count)) == -1 && errno == EINTR);

	return ret_val;
}





int tty_reset()
{
	if(tcsetattr(ttyfd, TCSAFLUSH, &orig_termios) < 0)
		return -1;
	return 0;
}


void set_tty()
{
	struct termios raw;
	raw = orig_termios;

	raw.c_lflag &= ~(ECHO | ICANON | ISIG);
	raw.c_cc[VMIN] = 1;
	raw.c_cc[VTIME] = 1;
	
	if(tcsetattr(ttyfd, TCSAFLUSH, &raw) < 0)
		fatal("Can not set raw mode");
}


int get_cursor_pos(int *row, int *col)
{
	char buff[32];
	int n;
	char *startPos = &buff[2]; 
	char *endPos;

	if((n = safe_write(STDOUT_FILENO, "\033[6n", 4)) == -1)
		fatal("Error in writting 'read cursor position' command");
	
	if(n != 4)
		fatal2("'read cursor position' command is not written in entirety");

	if((n = safe_read(ttyfd, buff, 20)) == -1)
		fatal("Error in reading cursor position");
	
	if(n < 5)
	{
		//fatal2("Invalid response to cursor position read command");
		return -1;	
	}	
	//buff[n] = '\0';

	if(buff[0] != '\033' && buff[1] != '[')
		return -2;	
		//fatal2("Invalid reponse format to cursor position read command");

	*row = strtol(startPos, &endPos, 10);

	if(*row == 0 && startPos == endPos)
		return -3;
		//fatal2("Invalid response format to cursor position read command, expected number1");

	if(*endPos != ';')
		return -4;
		//fatal2("Invalid response format to cursor position read command, expected ';'");

	startPos = endPos + 1;

	*col = strtol(startPos, &endPos, 10);

	if(*col == 0 && startPos == endPos)
		return -5;	
		//fatal2("Invalid response format to cursor position read command, expected number2");

	if(*endPos != 'R' && buff + n == endPos + 1)
		return -6;	
		//fatal2("Invalid response format to cursor position read command, expected 'R'");

	return 0;
}



int get_terminal_dim(int *row, int *col)
{
	int status;

	safe_write(STDOUT_FILENO, "\0337", 2); // save cursor position
	safe_write(STDOUT_FILENO, "\033[1000;1000f", 12);
	status =  get_cursor_pos(row, col);
	safe_write(STDOUT_FILENO, "\0338", 2); // restor cursor position

	return status;
}



int display_msgs_term()
{

	int term_row, term_col;
	char buff[2048];
	size_t buff_len;
	int current_row;

	// clear screen and set cursor to the top of the screen
	safe_write(STDOUT_FILENO, eraseScreenCmd, 4);	
	safe_write(STDOUT_FILENO, startCursorPosCmd, 6);

	get_terminal_dim(&term_row, &term_col);

	current_row = term_row - 2;

	buff_len = sprintf(buff, "\033[%d;%df", current_row, 0);
	safe_write(STDOUT_FILENO, buff, buff_len);


	for(auto it = std::crbegin(messages); it != std::crend(messages); ++it)
	{
		size_t row_len = it->get_message().size() + it->get_user().size() + it->get_time().size() + 6;
		size_t msg_rows = row_len / term_col + 1; 
	
		current_row -= msg_rows + 1;

		if(current_row < 0)
			break;

		buff_len = sprintf(buff, "\033[%d;%df", current_row, 0);
		safe_write(STDOUT_FILENO, buff, buff_len);

		buff_len = sprintf(buff, "\033[32;1m[%s][%s]\033[0m: %s", 
											 it->get_user().c_str(), 
											 it->get_time().c_str(), 
											 it->get_message().c_str());
		safe_write(STDOUT_FILENO, buff, buff_len);
	}


	current_row = term_row - 2;
	buff_len = sprintf(buff, "\033[%d;%df", current_row, 0);
	safe_write(STDOUT_FILENO, buff, buff_len);

	buff_len = sprintf(buff, "\033[32;1m->\033[0m");
	safe_write(STDOUT_FILENO, buff, buff_len);

	return 0;
}




void display_messages()
{
	if(!isatty(ttyfd))
		fatal("STDIN is not connected to terminal");

	if(tcgetattr(ttyfd, &orig_termios) < 0)
		fatal("Can not get tty settings");

	set_tty();
	display_msgs_term();
	tty_reset();
}

