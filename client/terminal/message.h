#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>
#include <ctime>

class Message{
	public:
		Message(const std::string& u, const std::string& m, const time_t& t);

		const std::string& get_user() const { return user; }
		const std::string& get_message() const { return message; }
		time_t get_timestamp() const { return timestamp; }
		std::string get_time() const { return time; }

		bool operator<(const Message& m) const { return this->timestamp < m.timestamp; }

	private:
		std::string user;
		std::string message;
		std::string time;
		time_t timestamp;
};

#endif
