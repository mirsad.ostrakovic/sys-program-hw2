#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>
#include <ctime>

class Message{
	public:
		Message(const std::string& u, const std::string& m, const time_t& t);

		const std::string& getUser() const { return user; }
		const std::string& getMessage() const { return message; }
		time_t getTimestamp() const { return timestamp; }
		std::string getTime() const { return time; }

		bool operator<(const Message& m) const;

		static unsigned long idGenerator; 

	private:
		std::string user;
		std::string message;
		std::string time;
		time_t timestamp;
		unsigned long id;
};



#endif
