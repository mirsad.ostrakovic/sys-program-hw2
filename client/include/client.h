#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <unordered_set>
#include <mutex>
#include <algorithm>


#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>


#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "error.h"
#include "display.h"
#include "protocol.h"

#define OK 0
#define SOCKET_CLOSED -1
#define NEW_MESSAGE 1

#ifndef BUFF_SZ
	#define BUFF_SZ 1024
#endif

class ChatClient{
	public:
		ChatClient(const struct in_addr *ipAddr, int port);

	private:
		void clientMainLoop();
		void handleNewMessage();
		void processNewMessage(const char *buff);
		void handleUserInput();
		int getUserInput();

		int socketFD;		
		char userMsg[BUFF_SZ];
		size_t userMsgLen = 0u;
		
		static char inputBuff[BUFF_SZ];
		static size_t inputPos;
		
		friend class Display;
};

#endif
