#ifndef DISPLAY_H
#define DISPLAY_H

#include <set>
#include <cctype>
#include <vector>
#include <string>
#include <ctime>

#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "error.h"
#include "message.h"
#include "client.h"

#define BUFF_SZ 1024

extern std::set<Message> messages;

void handle_msg_from_server(int fd);
void handle_new_user_msg(int fd);
int get_user_input();

class Display{
	public:
		static void addNewMessage(const std::string& user, 
														  const std::string& msg, 
															int timestamp);

		static void displayMessages();
		static void refresh();
			
		static void setTTY();
		static int resetTTY();
	
	private:
		static int displayMessagesTerm();

		static ssize_t safeRead(int fd, void *buff, size_t count);
		static ssize_t safeWrite(int fd, const void *buff, size_t count);


		static int getCursorPos(int *row, int *col);
		static int getTerminalDim(int *row, int *col);


		static std::set<Message> messages;
		static struct termios origTermios;

		static bool isSetTTY;
		static int ttyFD;
		static const char eraseScreenCmd[];
		static const char startCursorPosCmd[];
};


#endif
