#include "message.h"

unsigned long Message::idGenerator = 0u; 

Message::Message(const std::string& u, const std::string& m, const time_t& t):
		user{u},
		message{m},
		timestamp{t},
		id{idGenerator++} 
{
	char buff[128];
	struct tm *timeInfo;

	timeInfo = gmtime(&timestamp);

	sprintf(buff, "%d:%d:%d %d.%d.%d", timeInfo->tm_hour, timeInfo->tm_min, timeInfo->tm_sec,
																		 timeInfo->tm_mday, timeInfo->tm_mon + 1, timeInfo->tm_year + 1900);
	time = buff;
}


bool Message::operator<(const Message& m) const 
{
	if(this->timestamp != m.timestamp)
		return this->timestamp < m.timestamp;
	else
		return this->id < m.id;
}

