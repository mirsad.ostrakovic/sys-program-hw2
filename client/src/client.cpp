#include "client.h"


using namespace rapidjson;


size_t ChatClient::inputPos = 0u;
char ChatClient::inputBuff[BUFF_SZ];

ChatClient::ChatClient(const struct in_addr *ipAddr, int port)
{
	struct sockaddr_in serverAddr;

	if(signal(SIGPIPE, SIG_IGN) == SIG_ERR)
		systemCallError("ChatClient::ChatClient(): Can not turn off SIGPIPE error");

	if((socketFD = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		systemCallError("ChatClient::ChatClient(): Can not get socket");

	memset(&serverAddr, 0, sizeof(serverAddr));

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(port);
	serverAddr.sin_addr = *ipAddr;

	if(connect(socketFD, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) == -1)
		systemCallError("ChatClient::ChatClient(): Can not connect to server");

	clientMainLoop();
}




void ChatClient::clientMainLoop()
{
	int maxFD;
	fd_set rfds;
	int rv;

	//write(STDOUT_FILENO, "before displayMessages()", strlen("before displayMessages()"));
	Display::displayMessages();
	//write(STDOUT_FILENO, "after displayMessages()", strlen("after displayMessages()"));

	while(true)
	{
		FD_ZERO(&rfds);
		FD_SET(socketFD, &rfds);
		FD_SET(STDIN_FILENO, &rfds);

		maxFD = socketFD > STDIN_FILENO ? socketFD : STDIN_FILENO;

		if((rv = select(maxFD + 1, &rfds, NULL, NULL, NULL)) == -1)
			systemCallError("ChatClient::clientMainLoop(): Server select on read sockets fail");
	
	
		for(int fd = 0; fd <= maxFD; ++fd)
		{	
			if(FD_ISSET(fd, &rfds))
			{	
				if(fd == socketFD)
				{
					handleNewMessage();
				}
				else if(fd == STDIN_FILENO)
				{
					handleUserInput();
				}
				else
					systemCallError("ChatClient::clientMainLoop(): select return unknown fd");
			}	
		}		
	
	}
}






void ChatClient::handleNewMessage()
{
	char msgBuff[BUFF_SZ];
	size_t msgLen;


	if(Protocol::readMessage(socketFD, (char *)&msgBuff, &msgLen) != 0)
	{
		reportError("ChatClient::handleNewMessage(): error in reading server message");
		Display::resetTTY();
		exit(-1);	
	}

	msgBuff[msgLen] = '\0'; // null terminating buffer

	processNewMessage(msgBuff);
	Display::refresh();
}




void ChatClient::processNewMessage(const char *buff)
{
	Document document;
	document.Parse(buff);

	if(!document.IsObject() || !document.HasMember("user") || 
		 !document.HasMember("message") || !document.HasMember("timestamp")
		)
		return;

	Value& user = document.HasMember("ip") ? document["ip"] : document["user"];
	Value& message = document["message"];
	Value& timestamp = document["timestamp"];

	if(user.IsString() && message.IsString() && timestamp.IsInt())
		Display::addNewMessage(user.GetString(), message.GetString(), timestamp.GetInt());
}





void ChatClient::handleUserInput()
{
	static const char messageFormat[] =   "{"    		
										 													"\"message\""    ":" 	 "\"%.*s\""
																		 		"}";

	char msgBuff[BUFF_SZ];
	size_t msgLen;


	if(getUserInput() != NEW_MESSAGE)
		return;

	msgLen = sprintf(msgBuff, messageFormat, userMsgLen, userMsg);

	//reportError(msgBuff);

	if(Protocol::sendMessage(socketFD, msgBuff, msgLen) != 0)
	{
		reportError("ChatClient::handleUserInput(): error in sending user message");
		return;
	}

}





int ChatClient::getUserInput()
{
	static const char deleteCharacterCmd[] = "\010\033[1P";

	int n;

	if((n = read(STDIN_FILENO, &inputBuff[inputPos], (BUFF_SZ - inputPos - 10 ? 1 : 0))) == -1)
		systemCallError("ChatClient::getUserInput(): error in reading data from socket");

	if(n == 0) 
		systemCallError("ChatClient::getUserInput(): problem with reading from STDIN");


	switch(inputBuff[inputPos])
	{
			
		
			case 27:
			 	Display::resetTTY();
			 	exit(0);

			case '\n':
			{
				if(inputPos != 0)
				{
					memcpy(userMsg, inputBuff, inputPos);
					userMsgLen = inputPos;
					inputPos = 0;	
					return NEW_MESSAGE;
				}
				else
					return 0;
		 	}

			case 127:
				if(inputPos > 0)
				{
					--inputPos;
					write(STDOUT_FILENO, deleteCharacterCmd, 5);
				}
				return 0;

			case '\t':
				inputBuff[inputPos++] = ' ';
				inputBuff[inputPos++] = ' ';
				write(STDOUT_FILENO, "  ", 2);
				return 0;
				
			default:	
				if(isalnum(inputBuff[inputPos]) || inputBuff[inputPos] == ' ')
					write(STDOUT_FILENO, &inputBuff[inputPos++], 1);
				return 0;
	}
}





