#include "display.h"


int Display::ttyFD = STDIN_FILENO;
const char Display::eraseScreenCmd[] = "\033[2J";
const char Display::startCursorPosCmd[] = "\033[1;1f";

bool Display::isSetTTY = false;
std::set<Message> Display::messages;
struct termios Display::origTermios;



void Display::addNewMessage(const std::string& user, const std::string& msg, int timestamp)
{
	messages.insert(Message(user, msg, timestamp));
}



void Display::displayMessages()
{
	
	if(!isSetTTY)
	{
		if(!isatty(ttyFD))
			systemCallError("STDIN is not connected to terminal");

		if(tcgetattr(ttyFD, &origTermios) < 0)
			systemCallError("Can not get tty settings");

		setTTY();
		isSetTTY = true;
	}
	
	displayMessagesTerm();
}



void Display::refresh()
{
	displayMessages();
}








ssize_t Display::safeRead(int fd, void *buff, size_t count)
{
	ssize_t retVal;

	while((retVal = read(fd, buff, count)) == -1 && errno == EINTR);

	return retVal;
}



ssize_t Display::safeWrite(int fd, const void *buff, size_t count)
{
	ssize_t retVal;

	while((retVal = write(fd, buff, count)) == -1 && errno == EINTR);

	return retVal;
}





int Display::resetTTY()
{
	if(tcsetattr(ttyFD, TCSAFLUSH, &origTermios) < 0)
		return -1;
	return 0;
}


void Display::setTTY()
{
	struct termios raw;
	raw = origTermios;

	raw.c_lflag &= ~(ECHO | ICANON | ISIG);
	raw.c_cc[VMIN] = 1;
	raw.c_cc[VTIME] = 1;
	
	if(tcsetattr(ttyFD, TCSAFLUSH, &raw) < 0)
		systemCallError("Can not set raw mode");
}


int Display::getCursorPos(int *row, int *col)
{
	char buff[32];
	int n;
	char *startPos = &buff[2]; 
	char *endPos;

	if((n = safeWrite(STDOUT_FILENO, "\033[6n", 4)) == -1)
		systemCallError("Error in writting 'read cursor position' command");
	
	if(n != 4)
		fatalError("'read cursor position' command is not written in entirety");

	if((n = safeRead(ttyFD, buff, 20)) == -1)
		fatalError("Error in reading cursor position");
	
	if(n < 5)
	{
		//fatalError("Invalid response to cursor position read command");
		return -1;	
	}	
	
	//buff[n] = '\0';

	if(buff[0] != '\033' && buff[1] != '[')
		return -2;	
		//fatalError("Invalid reponse format to cursor position read command");

	*row = strtol(startPos, &endPos, 10);

	if(*row == 0 && startPos == endPos)
		return -3;
		//fatalError("Invalid response format to cursor position read command, expected number1");

	if(*endPos != ';')
		return -4;
		//fatalError("Invalid response format to cursor position read command, expected ';'");

	startPos = endPos + 1;

	*col = strtol(startPos, &endPos, 10);

	if(*col == 0 && startPos == endPos)
		return -5;	
		//fatalError("Invalid response format to cursor position read command, expected number2");

	if(*endPos != 'R' && buff + n == endPos + 1)
		return -6;	
		//fatalError("Invalid response format to cursor position read command, expected 'R'");

	return 0;
}



int Display::getTerminalDim(int *row, int *col)
{
	int status;

	safeWrite(STDOUT_FILENO, "\0337", 2); // save cursor position
	safeWrite(STDOUT_FILENO, "\033[1000;1000f", 12);
	status = getCursorPos(row, col);
	safeWrite(STDOUT_FILENO, "\0338", 2); // restor cursor position

	return status;
}



int Display::displayMessagesTerm()
{

	int termRow, termCol;
	char buff[2048];
	size_t buffLen;
	int currentRow;

	// clear screen and set cursor to the top of the screen
	safeWrite(STDOUT_FILENO, eraseScreenCmd, 4);	
	safeWrite(STDOUT_FILENO, startCursorPosCmd, 6);

	getTerminalDim(&termRow, &termCol);

	currentRow = termRow - 2;

	buffLen = sprintf(buff, "\033[%d;%df", currentRow, 0);
	safeWrite(STDOUT_FILENO, buff, buffLen);


	for(auto it = std::crbegin(messages); it != std::crend(messages); ++it)
	{
		size_t rowLen = it->getMessage().size() + it->getUser().size() + it->getTime().size() + 6;
		size_t msgRows = rowLen / termCol + 1; 
	
		currentRow -= msgRows + 1;

		if(currentRow < 0)
			break;

		buffLen = sprintf(buff, "\033[%d;%df", currentRow, 0);
		safeWrite(STDOUT_FILENO, buff, buffLen);

		buffLen = sprintf(buff, "\033[32;1m[%s][%s]\033[0m: %s", 
											 it->getUser().c_str(), 
											 it->getTime().c_str(), 
											 it->getMessage().c_str());
		
		safeWrite(STDOUT_FILENO, buff, buffLen);
	}


	currentRow = termRow - 2;
	buffLen = sprintf(buff, "\033[%d;%df", currentRow, 0);
	safeWrite(STDOUT_FILENO, buff, buffLen);

	buffLen = sprintf(buff, "\033[32;1m->\033[0m");
	safeWrite(STDOUT_FILENO, buff, buffLen);


	safeWrite(STDOUT_FILENO, ChatClient::inputBuff, ChatClient::inputPos);

	return 0;
}



