#include <cctype>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "error.h"
#include "client.h"


bool isNumber(const char *s)
{
	while(*s != '\0')
		if(!isdigit(*(s++)))
			return false;
	return true;
}


int main(int argc, const char *argv[])
{

	struct in_addr ipAddr;
	int port;

	try{

		if(argc != 3)
			fatalError("main(): invalid number of arguments, program expects 2 argument");

		if(!isNumber(argv[1]))
			fatalError("main(): first argument is not number, but expected to be port number");

		if((port = atoi(argv[1])) < 0 || port > 65535)	
			fatalError("main(): first argument is not port number, port number has to be positive number less than 65536");

		if(inet_aton(argv[2], &ipAddr) == 0)	
			fatalError("main(): second argument is expected to be IP address, but it is not");


		ChatClient client(&ipAddr, port);

	}
	catch(std::exception& e)
	{
		std::cout << "CriticalError: " << e.what() << std::endl;
	}

	
	return 0;
}
