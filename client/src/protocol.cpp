#include "protocol.h"


int Protocol::sendData(int fd, const char *msgBuff, size_t msgLen)
{
	size_t n, pos = 0u;

	while(true)
	{
		if((n = write(fd, &msgBuff[pos], msgLen - pos)) == -1)
		{
			if(errno == EPIPE)
				return SOCKET_CLOSED; 
			else
				systemCallError("Protocol::sendData(): write error");		
		}

		if(n == 0)
			fatalError("Protocol::sendData(): write 0 bytes");

		pos += n;
	
		if(pos == msgLen)
			break;
	}

	return OK;
}




int Protocol::readData(int fd, char *msgBuff, size_t msgLen)
{
	int n, pos = 0;

	while(true)
	{
		if((n = read(fd, &msgBuff[pos], msgLen - pos)) == -1)
			systemCallError("Protocol::readData(): error in reading data from socket");

		if(n == 0)
			return SOCKET_CLOSED; 

		pos += n;

		if(pos == msgLen)
			return OK;	
	}
}





int Protocol::readMessage(int fd, char *msgBuff, size_t *msgLen)
{
	char lenBuff[LENGTH_FIELD_SZ+1];
	int readLen, buffLen = 0;
	
	char *lengthEndPos;
	int overreadLen;

	while(true)
	{
		if((readLen = read(fd, &lenBuff[buffLen], LENGTH_FIELD_SZ - buffLen)) == -1)
		{	
			if(errno == ECONNRESET)
				return SOCKET_CLOSED;

			systemCallError("Protocol::readMessage(): error in reading data from socket");
		}

		if(readLen == 0)
			return SOCKET_CLOSED;

		if((lengthEndPos = std::find(&lenBuff[buffLen], &lenBuff[buffLen + readLen], '\2')) != &lenBuff[buffLen + readLen])
		{
			buffLen += readLen;
			break;
		}	

		buffLen += readLen;;

		if(buffLen == LENGTH_FIELD_SZ)
			return INVALID_MSG_FORMAT;	
	}

	overreadLen = buffLen - (lengthEndPos - lenBuff) - 1;
	memcpy(msgBuff, lengthEndPos + 1, overreadLen);

	*msgLen = atoi(lenBuff);

	return readData(fd, &msgBuff[overreadLen], *msgLen - overreadLen);
}




int Protocol::sendMessage(int fd, const char *msg, size_t msgLen)
{
	char msgBuff[BUFF_SZ];

	if((msgLen = sprintf(msgBuff, "%d\2%.*s", msgLen, msgLen, msg)) <= 0)
		return FORMATTING_PROBLEM;

	//reportError(msgBuff);

	return sendData(fd, msgBuff, msgLen);	
}



